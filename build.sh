#!/bin/bash

# Copying Kernel Source
git clone --depth=1 https://github.com/MiCode/Xiaomi_Kernel_OpenSource.git -b tulip-p-oss Whyred
cd Whyred

# Downloading Toolchains
git clone --depth=1 https://android.googlesource.com/platform/prebuilts/gcc/linux-x86/aarch64/aarch64-linux-android-4.9 gcc
git clone --depth=1 https://android.googlesource.com/platform/prebuilts/gcc/linux-x86/arm/arm-linux-androideabi-4.9 gcc32
git clone --depth=1 https://android.googlesource.com/platform/prebuilts/clang/host/linux-x86 clang

# Downloading AnyKernel
git clone https://github.com/crazyuploader/AnyKernel3.git anykernel

export BRANCH="$(git rev-parse --abbrev-ref HEAD)"
export ZIPNAME="Kunnel.zip"
export KBUILD_BUILD_USER=Jungle
export KBUILD_BUILD_HOST=AMD

export KBUILD_COMPILER_STRING="Clang Version 9.0.3"
export ARCH=arm64 && export SUBARCH=arm64

curl -s -X POST https://api.telegram.org/bot${BOT_API_TOKEN}/sendMessage -d text="GitLab ${BRANCH} Build Started." -d chat_id=${KERNEL_CHAT_ID} -d parse_mode=HTML
START=$(date +"%s")
make O=out ARCH=arm64 whyred-perf_defconfig
make -j$(nproc --all) O=out ARCH=arm64 CC="$(pwd)/clang/clang-r353983c/bin/clang" CLANG_TRIPLE="aarch64-linux-gnu-" CROSS_COMPILE="$(pwd)/gcc/bin/aarch64-linux-android-" CROSS_COMPILE_ARM32="$(pwd)/gcc32/bin/arm-linux-androideabi-"
END=$(date +"%s")
DIFF=$((END - START))

if [ -f $(pwd)/out/arch/arm64/boot/Image.gz-dtb ]
	then
  	cp $(pwd)/out/arch/arm64/boot/Image.gz-dtb $(pwd)/anykernel
  	cd anykernel
  	zip -r9 ${ZIPNAME} *
	sha256sum ${ZIPNAME}
    echo "Build Finished in $((DIFF / 60)) minute(s) and $((DIFF % 60)) second(s)."
	curl -F chat_id="${KERNEL_CHAT_ID}" -F document=@"$(pwd)/${ZIPNAME}" https://api.telegram.org/bot${BOT_API_TOKEN}/sendDocument
else
	curl -s -X POST https://api.telegram.org/bot${BOT_API_TOKEN}/sendMessage -d text="GitLab ${BRANCH} Build finished with errors... in $((DIFF / 60)) minute(s) and $((DIFF % 60)) second(s)." -d chat_id=${KERNEL_CHAT_ID} -d parse_mode=HTML
  	echo "Built with errors!"
  	exit 1
fi
